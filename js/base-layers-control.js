app.BaseLayersControl = function (opt_options) {
    var options = opt_options || {};

    var legends = options.legends;
    var fragment = $(document.createDocumentFragment());
    var that = this;
    
	
	var $button = $("<button type='button' class='glyphicon glyphicon-list' title='Choose base layer'>");
	var that = this;
    var $layersPanel = $("<div class='base-layers-panel list-group'>");
    for (var i = 0; i < legends.length; i++) {
        var legend = legends[i];
        var $legendItem = $('<a href="#" class="list-group-item list-group-item-action base-layer-item" layer="'+legend.layerName+'">'+legend.layerText+'</a>');
        $layersPanel.append($legendItem);
    }

    $layersPanel.delegate(".base-layer-item", "click", function () {
        var $this = $(this);
        if ($this.hasClass("active")) {
            return;
        }

        if (that._inited == true) {
            var map = that.getMap();
            map.deactivateControls();
        }

        $layersPanel.find(".base-layer-item").removeClass("active");
        $this.addClass("active");
        var layerName = $(this).attr('layer');
		for (var i = 0; i < legends.length; i++){
			var legend = legends[i];
			legend.layer.set('visible',false);
				
			if(legend.layerName==layerName){
				legend.layer.set('visible',true);
			}
		}
        $layersPanel.hide();
    });
	$($layersPanel.find(".base-layer-item")[0]).click();
  
	$('body').click(function (evt) {
        if ($layersPanel.is(':visible') == false) {
            return;
        }
        if ($(evt.target).parents('.base-layers-panel').length == 0) {
            $layersPanel.hide();
        }
    });
	
	var $element = $("<div class='base-layers-control ol-unselectable ol-control'>");
    $element.append($button);

    $button.click(function () {
        $element.after($layersPanel);
        $layersPanel.show();
        that._inited = true;
    });

    this.onControlsDeactivate = function (sender) {
        if (sender == that) {
            return;
        }
        if (that._inited != true) {
            return;
        }
        $layersPanel.hide();
    };

    ol.control.Control.call(this, {
        element: $element[0],
        target: options.target
    });
};
ol.inherits(app.BaseLayersControl, ol.control.Control);