﻿ol.Map.prototype.deactivateControls = function(sender) {
    var map = this;
    var controls = map.getControls();
    for(var i=0;i<controls.getLength();i++) {
        var curControl = controls.item(i);
        if(curControl.onControlsDeactivate) {
            curControl.onControlsDeactivate(sender);
        }
    }
};

var center = [-7908084, 6177492];

	// This dummy layer tells Google Maps to switch to its default map type


var bingTileLayerSat = new ol.layer.Tile({
		visible: false,
		preload: Infinity,
		source: new ol.source.BingMaps({
			key: 'Al6d3X68zuqRuuN4vjmSyzMexRApdetE7N6mr5Iae-XyGMn8IxDhGnz0xQhJZk5n',
			imagerySet: 'Aerial'
		})
	});
	var googleLayerSat = new olgm.layer.Google({ mapTypeId: google.maps.MapTypeId.SATELLITE });
    googleLayerSat.setVisible(false);
    var googleLayerRoad = new olgm.layer.Google({ mapTypeId: google.maps.MapTypeId.ROADMAP });
    googleLayerRoad.setVisible(false);
    var googleLayerHybrid = new olgm.layer.Google({ mapTypeId: google.maps.MapTypeId.HYBRID });
    googleLayerHybrid.setVisible(false);
	
	var bingTileLayerHybrid = new ol.layer.Tile({
		visible: false,
		preload: Infinity,
		source: new ol.source.BingMaps({
			key: 'Al6d3X68zuqRuuN4vjmSyzMexRApdetE7N6mr5Iae-XyGMn8IxDhGnz0xQhJZk5n',
			imagerySet: 'AerialWithLabels'
		})
	});
	var osmTileLayer = new ol.layer.Tile({
        visible: false,
		source: new ol.source.OSM()
    });

	var legends = [
		{
			layer: bingTileLayerSat,
			layerName: 'bing-sat',
			layerText: 'Bing Satellite Layer',
			//isGoogleLayer:false
		},{
			layer: bingTileLayerHybrid,
			layerName: 'bing-hybrid',
			layerText: 'Bing Hybrid Layer',
			//isGoogleLayer:false
		},{
			layer: osmTileLayer,
			layerName: 'osm',
			layerText: 'OpenStreetMap Layer',
			//isGoogleLayer:false
		},{
			layer: googleLayerSat,
			layerName: 'google-sat',
			layerText: 'Google Layer Satellite',
			//isGoogleLayer:true
		},{
			layer: googleLayerHybrid,
			layerName: 'google-hybrid',
			layerText: 'Google Layer Hybrid',
			//isGoogleLayer:true
		}
	];


var map = new ol.Map({
  // use OL3-Google-Maps recommended default interactions
  loadTilesWhileAnimating: true,
  loadTilesWhileInteracting: true,
  interactions: olgm.interaction.defaults(),
  layers: [
    googleLayerHybrid,googleLayerSat,osmTileLayer,bingTileLayerHybrid,bingTileLayerSat
  ],
  target: 'map',
  view: new ol.View({
    center: center,
    zoom: 12
  }),
  controls:[new ol.control.Zoom(),new app.BaseLayersControl({legends:legends})]
});

var geocoder = new Geocoder('nominatim', {
    provider: 'google',
	key:'AIzaSyBTECdNQlfcUI0U1a2KydhMWv8TN1a_3H4',
	lang: 'en-US',
    placeholder: 'Search for ...',
    limit: 10,
    keepOpen: false,
	autoComplete:true
  });
  map.addControl(geocoder);
  
  //Listen when an address is chosen
  geocoder.on('addresschosen', function(evt){
    var feature = evt.feature,
        coord = evt.coordinate;

    content.innerHTML = '<p>'+ evt.address.formatted +'</p>';
    overlay.setPosition(coord);
  });


  /**
  * Popup
  **/
  var container = document.getElementById('popup'),
      content = document.getElementById('popup-content'),
      closer = document.getElementById('popup-closer'),
      overlay = new ol.Overlay({
        element: container,
        offset: [0, -40]
      });
  closer.onclick = function() {
    overlay.setPosition(undefined);
    closer.blur();
    return false;
  };
  map.addOverlay(overlay);

var olGM = new olgm.OLGoogleMaps({map: map}); // map is the ol.Map instance
olGM.activate();
